# Getting Started
<br>

- Download the text editor of your choice
  - [VS Code](https://code.visualstudio.com/download)
  - [Atom](https://atom.io/)
  - [Sublime Text](https://www.sublimetext.com/3)
  - etc..

<br>

- Basic familiarity with how to navigate the terminal
    - Windows Users: I highly suggest downloading a console emulator, (I prefer [CMDER](http://cmder.net/))
    - Refer to this [cheat sheet](http://newsourcemedia.com/blog/basic-terminal-commands/) for help terminal commands

<br>

- Create a *free* [Gitlab](https://gitlab.com/users/sign_in#register-pane) account

<br>
<br>
<br>

## [Configuring SSH](./ssh-config.md)
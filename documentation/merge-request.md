## Merge Requests
After implementing all your new changes, an MR (Merge Request) should be submitted to the Director of Technology (or other repo owner). A merge request allows your code to be reviewed before being implemeted on the develop (gold standard) branch. This can be done through gitlab by:
1. Selecting "Create Merge Request" in Gitlab
2. Fill in the title and description fields with the appropriate information
3. Assign Gabe (@gbaduqui) as the *assignee*
3. Ensure your branch is set as the *Source branch* and **develop** is set as the *Target branch* (Merge requests should only be implemeneted into the 'dev' branch, not 'master')
5. Select both check boxes (Remove source branch, squash commits...)
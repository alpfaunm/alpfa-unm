# Configuring SSH

<br>

An SSH Key authenticates a remote computer and user, allowing you to submit changes to the repo. Follow the steps below to create a new SSH Key.

<br>

### 1. Generate a new SSH Key pair  
Open the terminal and use the command:  
```
ssh-keygen -t rsa -C "your.email@example.com" -b 4096
```  
This command generates a brand new SSH key pair. Press enter to place the SSH key in the suggested path location.
<br>
Next you will be prompted to input a password. It is best practice to use one, but you may skip this by pressing enter.

<br>
<br>

### 2. Add the SSH Key to your Gitlab account
Copy your newly generated public SSH Key to your clipboard:
<br>

**MacOS**  
```pbcopy < ~/.ssh/id_rsa.pub```  

**Windows**  
```type %userprofile%\.ssh\id_rsa.pub | clip```  

Add the key to Gitlab

- Assure you are signed in to Gitlab
- Inside Gitlab, click "Settings" under your Profile dropdown  
- Select "SSH Keys" in the left sidebar  
- Paste (Ctrl/CMD + v) in the "key" textarea

<br>
<br>
<br>

## [Development Process](./documentation/process.md)
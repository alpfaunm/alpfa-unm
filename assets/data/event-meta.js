events = [
    //Event 1 - Dions
    {
        id: 1,
        title: 'Dions Convention Gathering',
        date: 'July 26',
        time: '5:30pm',
        location: 'Dions (Montgomery / Academy)',
        image: '',
        dress: 'Casual',
        description: 'Pre-convention planning and preparation!'
    },
    //Event 2 - Convention
    {
        id: 2,
        title: '2018 ALPFA National Convention',
        date: 'July 29 - August 2',
        time: '',
        location: 'Las Vegas Nevada',
        image: '',
        dress: 'Business Professional',
        description: 'Join thousands of fellow ALPFA professionals at ALPFA\'s premiere event!'
    },
    //Event 3 - ALPFA Orientation
    {
        id: 3,
        title: 'ALPFA Orientation',
        date: 'August 30',
        time: '5:30pm',
        location: 'Jackson Student Center',
        image: '',
        dress: 'Business Casual',
        description: 'Come learn about this prestigous organization and how you can become involved!'
    },
    //Event 4 - Tap That
    {
        id: 4,
        title: 'Student Social',
        date: 'September 6',
        time: '4:00pm',
        location: 'Tap That Craftroom',
        image: '',
        dress: 'Casual',
        description: 'Come relax and socialize with your fellow ALPFA UNM chapter members!'
    },
    //Event 5 - Mentorship Program Kickoff
    {
        id: 5,
        title: 'Mentorship Program Kickoff',
        date: 'September 20',
        time: '5:30pm',
        location: 'Sandbar Brewery',
        image: '',
        dress: 'Casual',
        description: 'Grow your network with professionals from the ALPFA Albuquerque chapter'
    },
    //Event 6 - Fall Frenzy
    {
        id: 6,
        title: 'Fall Frenzy',
        date: 'September 21',
        time: '9:00am',
        location: 'UNM Duck Pond',
        image: '',
        dress: 'Casual',
        description: 'Join fellow chapter members to give back to our community and enjoy free lunch!'
    },
    //Event 7 - September Chapter Meeting
    {
        id: 7,
        title: 'Monthly Chapter Meeting',
        date: 'September 28',
        time: '12:00pm',
        location: 'Jackson Student Center',
        image: '',
        dress: 'Business Casual',
        description: 'This month we will be hosting David Sayers from Accion, a non-profit group working in the high impact fields of community development and Microfinance. David will be speaking about the 5 C\'s of credit'
    },
    //Event 8 - Student/Professional Tailgate
    {
        id: 8,
        title: 'Student-Professional Tailgate',
        date: 'September 29',
        time: '12:00pm',
        location: 'South lot - ALPFA Albuquerque Tent',
        image: '',
        dress: 'Casual',
        description: 'Come join the professional chapter for UNM\'s next home game! Go \'Bos!'
    },
    //Event 9 - Student Social
    {
        id: 9,
        title: 'Student Social',
        date: 'October 19',
        time: '4:00pm',
        location: '',
        image: '',
        dress: 'Casual',
        description: 'Come relax and socialize with your fellow ALPFA UNM chapter members at this fun event!'
    },
    //Event 10 - Monthly Chapter Meeting
    {
        id: 10,
        title: 'Monthly Chapter Meeting',
        date: 'October 26',
        time: '12:00pm',
        location: 'Jackson Student Center',
        image: '',
        dress: 'Business Casual',
        description: ''
    },
    //Event 11 - Student/Professional Tailgate
    {
        id: 11,
        title: 'Student-Professional Tailgate',
        date: 'November 3',
        time: '12:00pm',
        location: 'South lot - ALPFA Albuquerque tent',
        image: '',
        dress: 'Casual',
        description: 'Come join the professional chapter for UNM\'s next home game! Go \'Bos!'
    },
    //Event 12 - Monthly Chapter Meeting
    {
        id: 12,
        title: 'Monthly Chapter Meeting',
        date: 'November 16',
        time: '12:00pm',
        location: 'Jackson Student Center',
        image: '',
        dress: 'Business Casual',
        description: ''
    },
    //Event 13 - Roadrunner Foodbank
    {
        id: 13,
        title: 'Roadrunner Foodbank',
        date: 'December 1',
        time: '9:00am',
        location: 'Roadrunner Foodbank',
        image: '',
        dress: 'Casual',
        description: 'Come join your fellow chapter members and give back to the community this holiday season!'
    },
    //Event 14 - Student Social
    {
        id: 14,
        title: 'Student Social',
        date: 'December 1',
        time: '12:00pm',
        location: '',
        image: '',
        dress: 'Casual',
        description: 'Lets relax and socialize following Roadrunner foodbank!'
    },
]
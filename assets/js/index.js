//Date variables
var today = new Date();
var month = today.getMonth();
var date = today.getDate();

var dateNum = month + dateNum;

//Universal image route
var source = './assets/images/index/';

//Event Arrays
var titles = [
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    'Student-Professional Tailgate',
    'Student Social',
    'Chapter Meeting',
    'Student-Professional Tailgate',
    'Chapter Meeting',
    'Roadrunner Foodbank',
    'Student Social',
    'Monthly Chapter Meeting',
    'Monthly Chapter Meeting',
    'Monthly Chapter Meeting',
    'Monthly Chapter Meeting'
];

var images = [
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    'tailgate.jpg',
    'round1.jpg',
    'northwestern.jpg',
    'tailgate18.jpg',
    'deptState.jpg',
    'roadrunner.png',
    'studentSocial.jpg',
    '',
    '',
    '',
    ''
];

//Code to render next event based on DATE
var nextTitle;
var nextImage;


//Render September events
if (month === 8) {
    nextTitle = title[7];
    nextImage = source + image[7];
}

//Render October events
if (month === 9) {

    if (date <= 19) {
        nextTitle = titles[7];
        nextImage = source + images[7];
    }

    if (date <= 26) {
        nextTitle = titles[8];
        nextImage = source + images[8];
    }

}//End October Events

//Render November events
if (month === 10) {

    if (date <= 3) {
        nextTitle = titles[9];
        nextImage = source + images[9];
    }

    if (date <= 16) {
        nextTitle = titles[10];
        nextImage = source + images[10];
    }

}//End November events

//Render December events
if (month === 11) {

    if (date <= 1) {
        nextTitle = titles[11];
        nextImage = source + images[11];
    }

    if (date <= 1) {
        nextTitle = titles[12];
        nextImage = source + images[12];
    }

}

//Set Title/Image for next event
document.getElementById('iimage').src = nextImage;
document.getElementById('title').innerHTML = nextTitle;
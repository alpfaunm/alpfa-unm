$(document).ready( () => {
    $('#calendar').fullCalendar({

        height: 40,

        themeSystem: 'bootstrap4',

        header: {
            right: 'prev, next'
        },
        
        weekends: false,
        eventColor: '#558fed',

        //Events Array
        events: [
            //Event 1
            {
                title: 'Dions Convention Gathering',
                start: '2018-07-26T17:30:00',
                end:   '2018-07-26T19:00:00',
                allDay: false
            },
            //Event 2
            {
                title: '2018 ALPFA National Convention',
                start: '2018-07-29',
                end:   '2018-08-02'
            },
            //Event 3
            {
                title: 'ALPFA Orientation',
                start: '2018-08-30T17:30:00',
                end:   '2018-08-30T19:00:00',
                allDay: false
            },
            //Event 4
            {
                title: 'Student Social - Tap That Craftroom',
                start: '2018-09-07T16:00:00',
                end: '2018-09-07T18:00:00',
                allDay: false
            },
            //Event 5
            {
                title: 'Mentorship Program Kickoff',
                start: '2018-09-20T17:30:00',
                end: '2018-09-20T19:00:00',
                allDay: false
            },
            //Event 6
            {
                title: 'Fall Frenzy',
                start: '2018-09-21T09:00:00',
                end: '2018-09-21T13:00:00',
                allDay: false
            },
            //Event 7
            {
                title: 'Chapter Meeting',
                start: '2018-09-28T12:00:00',
                end: '2018-09-28T13:30:00',
                allDay: false
            },
            //Event 8
            {
                title: 'Student-Professional Tailgate',
                start: '2018-09-29T11:00:00',
                end: '2018-09-29T14:00:00',
                allDay: false
            },
            //Event 9
            {
                title: 'Student Social',
                start: '2018-10-19T16:00:00',
                end: '2018-10-19T18:00:00',
                allDay: false
            },
            //Event 10
            {
                title: 'Chapter Meeting',
                start: '2018-10-26T12:00:00',
                end: '2018-10-26T13:30:00',
                allDay: false
            },
            //Event 11
            {
                title: 'Student-Professional Tailgate',
                start: '2018-11-03T11:00:00',
                end: '2018-11-03T14:00:00',
                allDay: false
            },
            //Event 12
            {
                title: 'Chapter Meeting',
                start: '2018-11-16T12:00:00',
                end: '2018-11-16T13:30:00',
                allDay: false
            },
            //Event 13
            {
                title: 'Roadrunner Foodbank',
                start: '2018-12-01T09:00:00',
                end: '2018-12-01T11:00:00',
                allDay: false
            },
            //Event 14
            {
                title: 'Student Social',
                start: '2018-12-01T16:00:00',
                end: '2018-12-01T18:00:00',
                allDay: false
            },
            
        ]

        //Events JSON link
        //events: '../data/events.json'

    })
});
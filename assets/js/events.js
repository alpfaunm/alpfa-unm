//Date variables
var today = new Date();
var month = today.getMonth();
var date = today.getDate();

var dateNum = month + dateNum;

//Universal image route
var source = '../assets/images/events/';

//Event Arrays
var titles = [
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    'Student-Professional Tailgate',
    'Student Social',
    'Chapter Meeting',
    'Student-Professional Tailgate',
    'Chapter Meeting',
    'Roadrunner Foodbank',
    'Student Social',
    'Monthly Chapter Meeting',
    'Monthly Chapter Meeting',
    'Monthly Chapter Meeting',
    'Monthly Chapter Meeting'
];

var images = [
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    'tailgate.jpg',
    'round1.jpg',
    'northwestern.jpg',
    'tailgate18.jpg',
    'deptState.jpg',
    'roadrunner.png',
    'studentSocial.jpg',
    '',
    '',
    '',
    ''
];

//Event dates
dates = [
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    'September 29 - ',
    'October 19 - ',
    'October 26 - ',
    'November 3 - ',
    'November 16 - ',
    'December 1 - ',
    'December 1 - ',
    'January 25 - ',
    'February 22 - ',
    'March 22 - ',
    'April 19 - '
];

//Event times
times = [
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '12:00pm',
    '4:00pm',
    '12:00pm',
    '12:00pm',
    '12:00pm',
    '9:00am',
    '12:00pm',
    '12:00pm',
    '12:00pm',
    '12:00pm',
    '12:00pm'
];

//Event Locations
locations = [
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    'South lot - ALPFA ABQ tent',
    'Round1',
    'Jackson Student Center',
    'South lot - ALPFA ABQ tent',
    'Jackson Student Center',
    'Roadrunner Foodbank of New Mexico',
    '',
    'Jackson Student Center',
    'Jackson Student Center',
    'Jackson Student Center',
    'Jackson Student Center'
];

//Event Dress codes
dressCodes = [
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    'Casual',
    'Casual',
    'Business Casual',
    'Casual',
    'Business Casual',
    'Casual',
    'Casual',
    'Business Casual',
    'Business Casual',
    'Business Casual',
    'Business Casual'
];

//Event Descriptions
descriptions = [
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    'Come join the professional chapter for UNM\'s next home game! Go \'Bos!',
    'Come relax and socialize with your fellow ALPFA UNM chapter members at this fun event!',
    'Northwestern Mutual',
    'Come join the professional chapter for UNM\'s next home game! Go \'Bos!',
    '',
    'Come join your fellow chapter members and give back to the community this holiday season!',
    'Lets relax and socialize following Roadrunner foodbank!',
    'Convention Overview',
    'Honeywell',
    'Pepsi Co.',
    'Accounting firm'
];


//Code to render next event based on DATE
var nextTitle;
var nextDate;
var nextTime;
var nextLocation;
var nextImage;
var nextDress;
var nextDesc;


//Render September events
if (month === 8) {
    nextTitle = titles[7];
    nextDate = dates[7];
    nextTime = times[7];
    nextLocation = locations[7];
    nextImage = source + images[7];
    nextDress = dressCodes[7];
    nextDesc = descriptions[7];
}

//Render October events
if (month === 9) {

    if (date <= 19) {
        nextTitle = titles[7];
        nextDate = dates[7];
        nextTime = times[7];
        nextLocation = locations[7];
        nextImage = source + images[7];
        nextDress = dressCodes[7];
        nextDesc = descriptions[7];
    }

    if (date <= 26) {
        nextTitle = titles[8];
        nextDate = dates[8];
        nextTime = times[8];
        nextLocation = locations[8];
        nextImage = source + images[8];
        nextDress = dressCodes[8];
        nextDesc = descriptions[8];
    }

}//End October Events

//Render November events
if (month === 10) {

    if (date <= 3) {
        nextTitle = titles[9];
        nextDate = dates[9];
        nextTime = times[9];
        nextLocation = locations[9];
        nextImage = source + images[9];
        nextDress = dressCodes[9];
        nextDesc = descriptions[9];
    }

    if (date <= 16) {
        nextTitle = titles[10];
        nextDate = dates[10];
        nextTime = times[10];
        nextLocation = locations[10];
        nextImage = source + images[10];
        nextDress = dressCodes[10];
        nextDesc = descriptions[10];
    }

}//End November events

//Render December events
if (month === 11) {

    if (date <= 1) {
        nextTitle = titles[11];
        nextDate = dates[11];
        nextTime = times[11];
        nextLocation = locations[11];
        nextImage = source + images[11];
        nextDress = dressCodes[11];
        nextDesc = descriptions[11];
    }

    if (date <= 1) {
        nextTitle = titles[12];
        nextDate = dates[12];
        nextTime = times[12];
        nextLocation = locations[12];
        nextImage = source + images[12];
        nextDress = dressCodes[12];
        nextDesc = descriptions[12];
    }

}

//Set Title/Image for next event
document.getElementById('cardImg').src = nextImage;
document.getElementById('cardTitle').innerHTML = nextTitle;
document.getElementById('cardDate').innerHTML = nextDate;
document.getElementById('cardTime').innerHTML = nextTime;
document.getElementById('cardLocation').innerHTML = nextLocation;
document.getElementById('cardDress').innerHTML = nextDress;
document.getElementById('cardDesc').innerHTML = nextDesc;


//Fall semester events
for (i = 8; i < 14; i++) {
    loopTitle = 'eventTitle' + i;
    loopDate = 'eventDate' + i;
    loopTime = 'eventTime' + i;
    loopLocation = 'eventLocation' + i;
    loopDress = 'eventDress' + i;
    loopDesc = 'eventDesc' + i;
    loopImg = 'eventImg' + i;

    document.getElementById(loopTitle).innerHTML = titles[i];
    document.getElementById(loopDate).innerHTML = dates[i];
    document.getElementById(loopTime).innerHTML = times[i];
    document.getElementById(loopLocation).innerHTML = locations[i];
    document.getElementById(loopDress).innerHTML = dressCodes[i];
    document.getElementById(loopDesc).innerHTML = descriptions[i];
    document.getElementById(loopImg).src = source + images[i];
}

// document.getElementById('eventTitle8').innerHTML = title8;
// document.getElementById('eventDate8').innerHTML = date8;
// document.getElementById('eventTime8').innerHTML = time8;
// document.getElementById('eventLocation8').innerHTML = location8;
// document.getElementById('eventDress8').innerHTML = dress8;
// document.getElementById('eventDesc8').innerHTML = desc8;
// document.getElementById('eventImg8').src = source + image8;

// document.getElementById('eventTitle9').innerHTML = title9;
// document.getElementById('eventDate9').innerHTML = date9;
// document.getElementById('eventTime9').innerHTML = time9;
// document.getElementById('eventLocation9').innerHTML = location9;
// document.getElementById('eventDress9').innerHTML = dress9;
// document.getElementById('eventDesc9').innerHTML = desc9;
// document.getElementById('eventImg9').src = source + image9;

// document.getElementById('eventTitle10').innerHTML = title10;
// document.getElementById('eventDate10').innerHTML = date10;
// document.getElementById('eventTime10').innerHTML = time10;
// document.getElementById('eventLocation10').innerHTML = location10;
// document.getElementById('eventDress10').innerHTML = dress10;
// document.getElementById('eventDesc10').innerHTML = desc10;
// document.getElementById('eventImg10').src = source + image10;

// document.getElementById('eventTitle11').innerHTML = title11;
// document.getElementById('eventDate11').innerHTML = date11;
// document.getElementById('eventTime11').innerHTML = time11;
// document.getElementById('eventLocation11').innerHTML = location11;
// document.getElementById('eventDress11').innerHTML = dress11;
// document.getElementById('eventDesc11').innerHTML = desc11;
// document.getElementById('eventImg11').src = source + image11;

// document.getElementById('eventTitle12').innerHTML = title12;
// document.getElementById('eventDate12').innerHTML = date12;
// document.getElementById('eventTime12').innerHTML = time12;
// document.getElementById('eventLocation12').innerHTML = location12;
// document.getElementById('eventDress12').innerHTML = dress12;
// document.getElementById('eventDesc12').innerHTML = desc12;
// document.getElementById('eventImg12').src = source + image12;

// document.getElementById('eventTitle13').innerHTML = title13;
// document.getElementById('eventDate13').innerHTML = date13;
// document.getElementById('eventTime13').innerHTML = time13;
// document.getElementById('eventLocation13').innerHTML = location13;
// document.getElementById('eventDress13').innerHTML = dress13;
// document.getElementById('eventDesc13').innerHTML = desc13;
// document.getElementById('eventImg13').src = source + image13;

// document.getElementById('eventTitle14').innerHTML = title14;
// document.getElementById('eventDate14').innerHTML = date14;
// document.getElementById('eventTime14').innerHTML = time14;
// document.getElementById('eventLocation14').innerHTML = location14;
// document.getElementById('eventDress14').innerHTML = dress14;
// document.getElementById('eventDesc14').innerHTML = desc14;
// document.getElementById('eventImg14').src = source + image14;
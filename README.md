# ALPFA UNM website repo
> Help on how to use Git and contribute to the webpage

<br>

## Purpose
In addition to promoting ALPFA UNM across campus, this project serves as a way to mimic working in a real-world web development environment, using intro-level technologies. Use this as an opportunity to practice, make mistakes and grow.

<br>
<br>

## How to contribute
- [Getting Started](./documentation/pre-reqs.md)
- [Configuring SSH](./documentation/ssh-config.md)
- [Development Process](./documentation/process.md)
- [Merge Requests](./documentation/merge-request.md)

<br>
<br>

## Current Technology (Fall 2018)
As of October 2018, the project contains basic front-end technologies (html, css, js, bootstrap). The mailchimp subscirber-adding service is currently being implemented.
<br>
<br>
UNM IT currently only supports PHP (....I know...sigh...) and MySQL on the back-end for custom web applications. I highly encourage future ALPFA UNM chapters to exlpore newer tech as UNM IT makesk them available.

<br>
<br>

#### File Structure
```
|-- _.gitlab
   +-- merge-request-template.md
|-- _assets
   +-- css
   +-- data
   +-- images
     |-- about
     |-- events
     |-- gallery
     |-- index
     |-- members    
   +-- js
   +-- alpfa-icon.ico
|-- documentation
   +-- merge-request.md
   +-- pre-reqs.md
   +-- process.md
   +-- ssh-config.md
|-- links
   +-- about
   +-- events
   +-- gallery
   +-- index
   +-- members
|-- index.html
|-- README.html
```